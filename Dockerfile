#FROM image-registry.openshift-image-registry.svc:5000/openshift/jenkins-agent-maven:v4.0
FROM image-registry.openshift-image-registry.svc:5000/openshift/java@sha256:c343983d08baf2b3cc19483734808b07523a0860a5b17fdcb32de2d1b85306ca
USER root
WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package

ENV PORT 5000
EXPOSE $PORT
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
